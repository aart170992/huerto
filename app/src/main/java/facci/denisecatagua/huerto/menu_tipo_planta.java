package facci.denisecatagua.huerto;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import facci.denisecatagua.huerto.entidades.hortalizas;


public class menu_tipo_planta  extends AppCompatActivity  {
    Button btn_hortalizas;
    menu_tipo_planta esta_actividad;
    private FirebaseAnalytics mFirebaseAnalytics;
    ArrayList<hortalizas> ls_hortalizas = new ArrayList<hortalizas>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_tipo_planta);
        this.setTitle("Menu Principal");
        esta_actividad = this;
        btn_hortalizas = (Button) findViewById(R.id.btn_hortaliza);
        btn_hortalizas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ls_hortalizas = new ArrayList<hortalizas>();
                //Intent intent = new Intent(esta_actividad, menu_tipo_planta.class);
                //startActivity(intent);
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(view.getContext());
                Bundle bundle = new Bundle();
                bundle.putString("mensaje", "Entrar a hortalizas");
                mFirebaseAnalytics.logEvent("Hortalizas", bundle);


                FirebaseFirestore.getInstance().collection("hortaliza")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d("TAG", document.getId() + " => " + document.getData());
                                        String obj = document.getData().toString();
                                        hortalizas obj_hortaliza = document.toObject(hortalizas.class);
                                        Log.d("nombre_", obj_hortaliza.getnombre().toString());
                                        ls_hortalizas.add(obj_hortaliza);
                                        Log.d("tamaño_hortalizas", String.valueOf(ls_hortalizas.size()));
                                    }
                                    Intent intent = new Intent(esta_actividad, menu_hortalizas.class);
                                    intent.putExtra("miLista", ls_hortalizas);
                                    startActivity(intent);
                                } else {
                                    Log.d("TAG","Error getting documents: ", task.getException());
                                }
                            }
                        });



                //DocumentReference docref = FirebaseFirestore.getInstance().collection("Huerto").document("p1J2Z73WXIoiMDQkyypB");
                //docref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                //  @Override
                //  public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                //      if(task.isSuccessful()){
                //          DocumentSnapshot doc = task.getResult();
                //          if(doc.exists()){
                //              String men = doc.getData().toString();
                //              Log.d("Document", doc.getData().toString());
                //              Toast.makeText(view.getContext(), doc.getData().toString(), Toast.LENGTH_SHORT).show();
                //          }else{
                //              Log.d("Document","No data");
                //          }
                //      }
                //  }
                //});
            }
        });
    }
}
package facci.denisecatagua.huerto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class menu_presentacion  extends AppCompatActivity {
    static menu_presentacion esta_actividad;
    Button btn_huerto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_presentacion);
        esta_actividad.setTitle("Menu Principal");
        setTheme(android.R.style.Theme);
        btn_huerto = (Button) findViewById(R.id.btn_huerta);


        btn_huerto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(esta_actividad, menu_tipo_planta.class);
                startActivity(intent);
            }
        });
        //btn_huerto.setOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View v) {
                //Toast.makeText(v.getContext(), "Seleccionó: ", Toast.LENGTH_SHORT).show();
                //Intent intent = new Intent(esta_actividad, menu_tipo_planta.class);
              //  startActivity(intent);

            //    //intent.putExtra(“msng”, MSNG);

          //  }
        //});
    }
}
